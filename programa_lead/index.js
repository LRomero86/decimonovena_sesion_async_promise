//const fetch = require("node-fetch");

function metodo_sincronico() {
    
    console.log("Me gusta");
    console.log("programar");
    console.log("con cerveza");
    console.log("todos los días");
    console.log("con mis perros");
    console.log("y gatos");

}

function metodo_asincronico() {
    
    console.log("NO me gusta");
    console.log("LOS DÍAS SIN PROGRAMAR");
    setTimeout(() => {
        console.log("con AGUA");
    }, 1000);

    setTimeout(() => {
        console.log("SIN cerveza");
    }, 2000);

    setTimeout(() => {
        console.log("SOLO LOS VIERNES");
    }, 3000);
    
    console.log("con mis perros");
    console.log("y gatos");

}




let promesa_ejecucion = new Promise((resolve, reject) => {
    
    setTimeout(() => {
        resolve('Promesa resuelta en 1 segundo')
    }, 1000);

    setTimeout(() => {
        reject('Promesa rechazada')
    }, 1500);    
});

// ATRAPAMOS LA PROMESA USANDO THEN & CATCH

promesa_ejecucion.then((mensaje) => {
    console.time("execution");
    console.log('Repsuesa es: ' + mensaje);
    console.timeEnd("execution");
}).catch((errorMessage) => {
    console.log('Error de promesa: ' + errorMessage);
});

// HACIENDO QUE INGRESE EN EL REJECTING

let promesa_rechazada = new Promise((resolve, reject) => {

    setTimeout(() => {
        resolve('Promesa ejecutada correctamente, seguro?');
    }, 1000);

    setTimeout(() => {
        reject('Promesa rechazada');
    }, 500);
});

promesa_rechazada.then((mensaje) => {
    console.log(mensaje);
}).catch((error) => {
    console.log(error);
});


// EJEMPLO DE PROMESA PENDIENTE QUE TERMINE DE RESOLVERSE

let promesa_pendiente = new Promise((resolve, reject) => {
    console.log('Pending...!!!');
    setTimeout(() => {
        if(true) {
            resolve('Promesa resuelta!');
        } else {
            reject('Promesa rechazada!');
        }
    }, 3500);
});

promesa_pendiente.then((successMessage) => {
    console.log('Respuesta de promesa pendiente: ' + successMessage);
}).catch((errorMessage) => {
    console.log('¡Error de promesa pendiente!' + errorMessage);
});


// EJEMPLO DE FETCH CON PROMESA PARA OBTENER UNA IMAGEN

let divImage = document.getElementById('imgDog');

function getDogImage(url) {
    
    console.log('Pendiente para la imagen');
    fetch(url)
        .then(response => response.json())
        .then(json => {
            let dogImage = document.createElement('img');
            dogImage.setAttribute('src', json.message);
            dogImage.style.width = '300px';
            divImage.appendChild(dogImage);
            console.log('Imagen cargada');
        })
        .catch(err => {
            console.log('Falló la comunicación al server' + error);
        });
}

getDogImage('https://dog.ceo/api/breeds/image/random');


metodo_sincronico();
metodo_asincronico();

const promesa_numero = new Promise(function (resolve, reject) {
    const numero = Math.floor(Math.random() * 10);
    setTimeout(() => {
        numero > 4 ? resolve(numero) : reject(new Error('Menor a 4: ' + numero));
    }, 2000);
});

promesa_numero
    .then(numero => console.log(numero))
    .catch(error => console.log(error));