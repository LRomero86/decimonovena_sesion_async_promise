const fetch = require('node-fetch');


function getGithubUser(username) {
    fetch('https://api.github.com/users/' + username)
    .then(response => response.json())
    .then(json =>  {
        console.log(json);
        getUserFirst5Followers(json.followers_url);
    }).catch(err => {
        console.error('Fetch failed', err);
    });
}

function getUserFirst5Followers(url) {
    fetch(url)
        .then(response => response.json())
        .then(jsondata => {
            console.log(jsondata);
            for (let i = 0; i < 5; i++) {
                
                console.log(jsondata[i].login);
                
            }
        }).catch(err => {
            console.error('fetch failed', err);
        })
}

getGithubUser('andrew');